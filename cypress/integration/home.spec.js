describe('Home', () => {
  it('something', () => {
    cy.visit('/');
    cy.matchImageSnapshot('home');
    cy.url().should('include', '/');
  });
});
