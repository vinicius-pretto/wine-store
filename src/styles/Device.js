const Device = {
  MOBILE_SMALL: "(max-width: 320px)",
  MOBILE_MEDIUM: "(max-width: 375px)",
  MOBILE: "(max-width: 425px)",
  TABLET: "(max-width: 768px)",
  TABLET_LARGE: "(max-width: 834px)",
  LAPTOP_EXTRA_SMALL: "(max-width: 1024px)",
  LAPTOP_SMALL: "(max-width: 1200px)",
  LAPTOP_LARGE: "(max-width: 1440px)",
  DESKTOP: "(max-width: 1900px)"
};

export default Device;
