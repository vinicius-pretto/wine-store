const theme = {
  fonts: {
    primary: 'Arial, Helvetica, sans-serif',
    fontSizeBase: '1rem',
    fontSizeSmall: '0.90rem'
  },
  colors: {
    primary: '#222',
    secondary: '#951672',
    white: '#FFF',
    gray: '#DEDEDE'
  },
  buttons: {
    primary: '#7FBC44'
  }
}

export default theme;
