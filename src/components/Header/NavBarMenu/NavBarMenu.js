import React from "react";
import styled from "styled-components";

import WineBox from "../WineBox";

const NavBar = styled.nav`
  display: flex;
  margin-left: 200px;
`
NavBar.displayName = 'NavBar';

const Menu = styled.ul`
  display: flex;
`;
Menu.displayName = "Menu";

const MenuItem = styled.li`
  margin-right: 10px;
`;
MenuItem.displayName = "MenuItem";

const MenuItemLink = styled.a`
  color: ${props => props.theme.colors.white};
  font-size: ${props => props.theme.fonts.fontSizeSmall};
  cursor: pointer;
`;
MenuItemLink.displayName = "MenuItemLink";

const NavBarMenu = () => {
  return (
    <React.Fragment>
      <NavBar>
        <Menu>
          <MenuItem>
            <MenuItemLink>Vinhos</MenuItemLink>
          </MenuItem>
        </Menu>
      </NavBar>
      <WineBox />
    </React.Fragment>
  );
};

export default NavBarMenu;
