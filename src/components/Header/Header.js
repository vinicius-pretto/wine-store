import React from 'react';
import styled from 'styled-components';
import Media from "react-media";

import MobileNavBarMenu from './MobileNavBarMenu';
import NavBarMenu from './NavBarMenu';
import Device from '../../styles/Device';

const StyledHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  padding: 20px;
  background-color: ${props => props.theme.colors.primary};

  @media ${Device.TABLET} {
    justify-content: space-between;
  }
`
StyledHeader.displayName = 'Header';

const Title = styled.h1`
  font-size: ${props => props.theme.fonts.fontSizeBase};
  color: ${props => props.theme.colors.white};
`
Title.displayName = 'Title';

const Header = () => {
  return (
    <StyledHeader>
      <Title>Wine Store</Title>
      <Media query={Device.TABLET}>
        {isTabletSize =>
          isTabletSize ? <MobileNavBarMenu /> : <NavBarMenu />
        }
      </Media>
    </StyledHeader>
  )
}

export default Header;
