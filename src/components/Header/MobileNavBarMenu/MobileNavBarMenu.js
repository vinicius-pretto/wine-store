import React from 'react';
import styled from 'styled-components';

import WineBox from '../WineBox';
import Menu from './Menu/Menu';

const NavMenu = styled.nav`
  display: flex;
  width: 85px;
  justify-content: space-between;
`
NavMenu.displayName = 'NavMenu';

class MobileNavBarMenu extends React.Component {
  render() {
    return (
      <NavMenu>
        <WineBox />
        <Menu />
      </NavMenu>
    )
  }
}

export default MobileNavBarMenu;
