import React from 'react';
import styled from 'styled-components';

import BurgerMenuIcon from '../../../../icons/BurgerMenuIcon';

const BurgerMenu = styled.button`
  display: flex;
  background: none;
  border: none;
`
BurgerMenu.displayName = 'BurgerMenu';

const Menu = () => {
  return (
    <BurgerMenu type="button">
      <BurgerMenuIcon />
    </BurgerMenu>
  )
}

export default Menu;
