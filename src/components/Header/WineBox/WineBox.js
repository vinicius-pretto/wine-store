import React from 'react';
import styled from 'styled-components';
import WineBoxIcon from '../../../icons/WineBoxIcon';
import Device from '../../../styles/Device';

const StyledWineBox = styled.a`
  top: 10px;
  right: 20px;
  position: absolute;

  @media ${Device.TABLET} {
    position: relative;
    right: 0;
    top: 0;
  }
`
StyledWineBox.displayName = 'WineBox';

const Counter = styled.span`
  position: absolute;
  top: 10px;
  left: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  background-color: ${props => props.theme.colors.secondary};
  color: ${props => props.theme.colors.white};
  font-size: ${props => props.theme.fonts.fontSizeSmall};
`
Counter.displayName = 'Counter';

const WineBox = () => {
  return (
    <StyledWineBox href="/">
      <WineBoxIcon />
      <Counter>0</Counter>
    </StyledWineBox>
  )
}

export default WineBox;
